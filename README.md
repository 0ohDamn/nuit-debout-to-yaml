# nuit-debout-to-yaml

Convert Nuit Debout catalog of services to a Git repository of YAML files.
Link to catalog : https://wiki.nuitdebout.fr/wiki/Ressources/Liste_d%27outils_num%C3%A9riques


## Usage

```bash
./nuit_debout_to_yaml.py yaml/
```
